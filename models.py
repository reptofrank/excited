from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
import datetime

# class Category(models.Model):
#     name = models.CharField(max_length=200)
#     amount = models.IntegerField(default=0)

class Post(models.Model):
    post_body = models.TextField()
    post_title = models.CharField(max_length=500)
    pub_date = models.DateTimeField('date published')
    author = models.CharField(max_length=500, default='Tochi Okafor')
    display = models.CharField(max_length=500)
    inner = models.CharField(max_length=500, null=True, blank=True)
    introduction = models.CharField(max_length=500, null=True)
    # picture = models.FileField(upload_to='blog/images')
    # category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
    	return self.post_title

    def was_published_recently(self):
    	return  timezone.now() - datetime.timedelta(days=1) <= self.pub_date <= timezone.now()
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class Inspired(models.Model):
    body = models.TextField()
    title = models.CharField(max_length=500)
    pub_date = models.DateTimeField(default=timezone.now)
    author = models.CharField(max_length=200, default='Tochi Okafor')


    def __str__(self):
        return self.title;