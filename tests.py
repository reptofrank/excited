import datetime

from django.test import TestCase
from django.utils import timezone

from .models import Post


class PostMethodTests(TestCase):
	"""docstring for PostMethodTests"""
	
	def test_was_published_recently(self):
		""" was_published_recently should return false if post in the future """

		time = timezone.now() + datetime.timedelta(days=30)
		future_post = Post(pub_date=time)
		self.assertIs(future_post.was_published_recently(), False)

	def test_was_published_recently_with_old_post(self):

		time = timezone.now() - datetime.timedelta(days=2)
		old_post = Post(pub_date=time)
		self.assertIs(old_post.was_published_recently(), False)

	def test_was_published_recently_with_recent_post(self):

		time = timezone.now() - datetime.timedelta(hours=2)
		recent_post = Post(pub_date=time)
		self.assertIs(recent_post.was_published_recently(), True)


