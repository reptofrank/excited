from django.contrib import admin

from .models import Post, Inspired

class PostAdmin(admin.ModelAdmin):
	list_display = ['post_title', 'author', 'pub_date', 'was_published_recently']
	list_filter = ['pub_date']
	search_fields = ['post_title']
	fieldsets = [
        (None,               {'fields': ['post_title', 'author', 'introduction', 'post_body']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
        ('Display Picture', {'fields': ['display', 'inner']}),
    ]


admin.site.register(Post, PostAdmin)
admin.site.register(Inspired)
