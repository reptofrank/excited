from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Post

def index(request):
	latest_posts = Post.objects.order_by('-pub_date')[:5]
	return render(request, 'blog/index.html', {'latest_posts': latest_posts})

def article(request, post_id):
	try:
		post = Post.objects.get(pk=post_id)
	except Post.DoesNotExist:
		raise Http404("Invalid Post")
	return render(request, 'blog/article.html', {'post': post})

def about(request):
	return render(request, 'main/about.html', {'title': 'About'})

def home(request):
	return render(request, 'main/index.html', {'title': 'Home'})


def contact(request):
	return render(request, 'main/contact.html', {'title': 'Contact'})